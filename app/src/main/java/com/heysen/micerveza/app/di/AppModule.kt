package com.heysen.micerveza.app.di

import com.heysen.micerveza.data.network.ClientNetwork
import com.heysen.micerveza.ui.MainViewModel
import com.heysen.micerveza.ui.detalle.DetalleCervezaViewModel
import com.heysen.micerveza.ui.home.HomeViewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val apiModules = module {
    factory { providePunkApi(get()) }
}

val repositoryModule = module {
    single { provideCervezaRepository(get()) }
}

val viewModelModule = module {
    factory { MainViewModel() }
    factory { HomeViewModel(get()) }
    factory { DetalleCervezaViewModel(get()) }
}

fun networkModule(): Module {
    return module {
        factory { ClientNetwork() }
    }
}