package com.heysen.micerveza.app.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.heysen.micerveza.R

fun<A : Activity> Activity.startNewActivity(activity: Class<A>) {
    Intent(this, activity).also {
        it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(it)
    }
}

fun ImageView.fromUrl(url: String?, mContext: Context?) {
    mContext?.let { context ->
        Glide.with(context)
            .load(url)
            .placeholder(R.drawable.ic_beer)
            .dontTransform()
            .into(this);
    }
}