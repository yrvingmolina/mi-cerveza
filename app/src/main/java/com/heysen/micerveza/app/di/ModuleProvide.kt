package com.heysen.micerveza.app.di

import com.heysen.micerveza.data.network.ClientNetwork
import com.heysen.micerveza.data.network.PunkApi
import com.heysen.micerveza.data.repository.CervezaRepository

fun provideCervezaRepository(
    api: PunkApi
): CervezaRepository {
    return CervezaRepository(api)
}

fun providePunkApi(
    clientNetwork: ClientNetwork
): PunkApi {
    return clientNetwork.buildApi(PunkApi::class.java)
}