package com.heysen.micerveza.ui.detalle

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.navArgs
import com.heysen.micerveza.R
import com.heysen.micerveza.app.util.fromUrl
import com.heysen.micerveza.data.network.Resource
import com.heysen.micerveza.data.repository.CervezaResponseItem
import com.heysen.micerveza.databinding.DetalleCervezaBinding
import com.heysen.micerveza.databinding.HomeBinding
import com.heysen.micerveza.ui.base.BaseFragment
import com.heysen.micerveza.ui.home.HomeViewModel
import com.heysen.micerveza.ui.home.ItemCervezaAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetalleCervezaFragment : BaseFragment<DetalleCervezaViewModel, DetalleCervezaBinding>() {

    override val viewModel: DetalleCervezaViewModel by viewModel()
    override fun getLayoutResId(): Int = R.layout.fragment_detalle_cerveza
    private lateinit var homeView: View
    private val args: DetalleCervezaFragmentArgs by navArgs()

    override fun init() {
        binding.apply {
            homeView            = root
            fragment            = this@DetalleCervezaFragment
            viewModel           = this@DetalleCervezaFragment.viewModel
            lifecycleOwner      = activity
        }
    }

    private var cerveza: CervezaResponseItem? = null

    override fun iniciarObservers(lifecycleOwner: LifecycleOwner) {
        viewModel.buscarCervezaPorId(args.cervezaId)

        viewModel.cerveza.observe(lifecycleOwner, {
            when (it) {
                is Resource.Success -> {
                    val lista: ArrayList<CervezaResponseItem> = it.datos
                    mostratCerveza(lista[0])
                }
                is Resource.Failure -> {
                    print(it)
                }
                else -> {
                    print(1)
                }
            }
        })
    }

    private fun mostratCerveza(item: CervezaResponseItem) {
        binding.apply {
            cerveza = item

            tvGrados.text = cerveza?.abv.toString()
            tvTitle.text = cerveza?.name
            tvSubtitle.text = cerveza?.tagline
            tvDescripcion.text = cerveza?.description
            tvConsejos.text = cerveza?.brewers_tips

            try {
                ivLogo.fromUrl(cerveza?.image_url, context)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}