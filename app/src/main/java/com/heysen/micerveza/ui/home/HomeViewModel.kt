package com.heysen.micerveza.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.heysen.micerveza.data.network.Resource
import com.heysen.micerveza.data.repository.CervezaRepository
import com.heysen.micerveza.data.repository.CervezaResponseItem
import com.heysen.micerveza.ui.base.BaseViewModel
import kotlinx.coroutines.launch

class HomeViewModel(private val repository: CervezaRepository): BaseViewModel() {

    private val _cervezas: MutableLiveData<Resource<ArrayList<CervezaResponseItem>>> = MutableLiveData()
    val cervezas: LiveData<Resource<ArrayList<CervezaResponseItem>>>
        get() = _cervezas

    fun buscarCervezasPorNombre(nombre: String? = null) {
        viewModelScope.launch {
            _cervezas.value = repository.obtenerCervezas(nombre)
        }
    }

}