package com.heysen.micerveza.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.heysen.micerveza.R
import com.heysen.micerveza.app.util.fromUrl
import com.heysen.micerveza.data.repository.CervezaResponseItem

class ItemCervezaAdapter(
    val items: MutableList<CervezaResponseItem>,
    val context: Context?,
    val listener: OnItemCervezaListener?,
    ): RecyclerView.Adapter<ItemCervezaAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.item_cerveza,
            parent,
            false
        )
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        fun bindItem(cerveza: CervezaResponseItem, position: Int) {
            var item = itemView.findViewById<ConstraintLayout>(R.id.cl_item_cerveza)
            var nombre = itemView.findViewById<TextView>(R.id.tv_nombre)
            var subTitulo = itemView.findViewById<TextView>(R.id.tv_sub_titulo)
            var descripcion = itemView.findViewById<TextView>(R.id.tv_descripcion)
            var logo = itemView.findViewById<ImageView>(R.id.iv_logo)

            nombre.text = cerveza.name
            subTitulo.text = cerveza.tagline
            descripcion.text = cerveza.brewers_tips

            try {
                logo.fromUrl(cerveza.image_url, context)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            item.setOnClickListener {
                listener?.onItemSelect(cerveza, position)
            }

        }
    }

    interface OnItemCervezaListener {
        fun onItemSelect(item: CervezaResponseItem?, position: Int)
    }
}