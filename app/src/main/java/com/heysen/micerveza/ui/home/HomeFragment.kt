package com.heysen.micerveza.ui.home

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.heysen.micerveza.R
import com.heysen.micerveza.data.network.Resource
import com.heysen.micerveza.data.repository.CervezaResponseItem
import com.heysen.micerveza.databinding.HomeBinding
import com.heysen.micerveza.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : BaseFragment<HomeViewModel, HomeBinding>() {

    override val viewModel: HomeViewModel by viewModel()
    override fun getLayoutResId(): Int = R.layout.fragment_home
    private lateinit var homeView: View

    override fun init() {
        binding.apply {
            homeView            = root
            fragment            = this@HomeFragment
            viewModel           = this@HomeFragment.viewModel
            lifecycleOwner      = activity
        }
    }

    private var listaCervezas: ArrayList<CervezaResponseItem>? = null
    private var adapter = ItemCervezaAdapter(arrayListOf(), null, null)

    override fun iniciarObservers(lifecycleOwner: LifecycleOwner) {
        viewModel.buscarCervezasPorNombre()

        viewModel.cervezas.observe(lifecycleOwner, {
            when (it) {
                is Resource.Success -> {
                    val lista: ArrayList<CervezaResponseItem> = it.datos
                    if (lista.size > 0) actualizarListaCervezas(lista)
                    else {
                        Toast.makeText(requireContext(),R.string.app_name,Toast.LENGTH_SHORT).show()
                    }
                }
                is Resource.Failure -> {
                    print(it)
                }
                else -> {
                    print(1)
                }
            }
        })
    }

    private fun actualizarListaCervezas(lista: ArrayList<CervezaResponseItem>) {
        binding.apply {
            listaCervezas = lista
            adapter = ItemCervezaAdapter(listaCervezas!!, requireContext(), listener)
            recyclerViewCervezas.adapter = adapter
        }
    }

    override fun configurarWidgets() {
        binding.apply {
            recyclerViewCervezas.layoutManager = LinearLayoutManager(requireContext())

            etNombreCerveza.addTextChangedListener(getTextWatcherNombre)
        }
    }

    private val getTextWatcherNombre = object : TextWatcher {
        var timer = Timer()
        override fun beforeTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) { }
        override fun afterTextChanged(s: Editable?) {
            timer.cancel()
            timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    val texto = s.toString().trim()
                    viewModel!!.buscarCervezasPorNombre(if (texto.isNotEmpty()) texto else null)
                }
            }, 1000)
        }
    }

    private val listener: ItemCervezaAdapter.OnItemCervezaListener = object : ItemCervezaAdapter.OnItemCervezaListener {
        override fun onItemSelect(item: CervezaResponseItem?, position: Int) {
            var action = HomeFragmentDirections.actionHomeFragmentToDetalleCervezaFragment(
                cervezaId = item!!.id
            )
            view?.findNavController()?.navigate(action)
        }
    }
}