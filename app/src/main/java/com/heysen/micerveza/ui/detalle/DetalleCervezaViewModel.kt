package com.heysen.micerveza.ui.detalle

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.heysen.micerveza.data.network.Resource
import com.heysen.micerveza.data.repository.CervezaRepository
import com.heysen.micerveza.data.repository.CervezaResponseItem
import com.heysen.micerveza.ui.base.BaseViewModel
import kotlinx.coroutines.launch

class DetalleCervezaViewModel(private val repository: CervezaRepository): BaseViewModel() {

    private val _cerveza: MutableLiveData<Resource<ArrayList<CervezaResponseItem>>> = MutableLiveData()
    val cerveza: LiveData<Resource<ArrayList<CervezaResponseItem>>>
        get() = _cerveza

    fun buscarCervezaPorId(id: Int) {
        viewModelScope.launch {
            _cerveza.value = repository.obtenerCerveza(id)
        }
    }

}