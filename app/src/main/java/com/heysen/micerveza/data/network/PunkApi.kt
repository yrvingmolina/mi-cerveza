package com.heysen.micerveza.data.network

import com.heysen.micerveza.data.repository.CervezaResponseItem
import retrofit2.http.GET
import retrofit2.http.Query

interface PunkApi {

    @GET("beers")
    suspend fun obtenerCervezas(@Query("beer_name") nombre: String? = null): ArrayList<CervezaResponseItem>

    @GET("beers")
    suspend fun obtenerCerveza(@Query("ids") id: String): ArrayList<CervezaResponseItem>

}