package com.heysen.micerveza.data.repository

import com.heysen.micerveza.data.network.PunkApi
import com.heysen.micerveza.data.network.Resource
import com.heysen.micerveza.data.network.SafeApiCall

class CervezaRepository(
    private val api: PunkApi
): SafeApiCall {

    suspend fun obtenerCervezas(nombre: String? = null): Resource<ArrayList<CervezaResponseItem>> = safeApiCall {
        api.obtenerCervezas(nombre)
    }

    suspend fun obtenerCerveza(id: Int): Resource<ArrayList<CervezaResponseItem>> = safeApiCall {
        api.obtenerCerveza(id.toString())
    }

}