package com.heysen.micerveza.data.network

import okhttp3.ResponseBody

sealed class Resource<out T> {

    data class Success<out T> (val datos: T, var funcion: (() -> Unit)? = null) : Resource<T>()
    data class Failure(
        val isNetworkError: Boolean,
        val errorCode: Int?,
        val errorBody: ResponseBody?
    ) : Resource<Nothing>()

    object Loading  : Resource<Nothing>()

    data class Stop(val mensajeId: Int): Resource<Nothing>()
    object Finish   : Resource<Nothing>()
    object Start    : Resource<Nothing>()
    object Close    : Resource<Nothing>()
    data class Continue(val mensajeId: Int): Resource<Nothing>()

}